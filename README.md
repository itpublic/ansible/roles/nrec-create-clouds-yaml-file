# nrec-create-clouds-yaml-file

Alternative to adding credentials to your environment on the command line (e.g. `source keystone_rc`).
This role can be used to get a more dynamic play when using ansible to create servers in nrec.

## Role Variables

Some variables are set in `defaults/main.yml`. Override them in your own `vars/main.yml`, `group_vars/groupname.yml`, `host_vars/hostname.yml` or similar. You need to specify you API username and password `os_username` and os_passasword` like this:
```
ansible-playbook site.yml -e "os_username=firstname.lastname@uib.no" -e "os_password=" ...
```
Or you can put your username and password in a `nrec_api_user_and_pass.yml` file (add to .gitignore or put outside your project, and set permissions only to yourself).
```
ansible-playbook site.yml -e "@nrec_api_user_and_pass.yml" 
```

## Dependencies
```
openstacksdk
openstacksdk >= 0.12.0
python >= 2.7
```

## Example Playbook
```
- hosts: localhost
  connection: local
  become: no
  gather_facts: no
  roles:
    - nrec-create-clouds-yaml-file
    - nrec-create-server # and or other roles
```

